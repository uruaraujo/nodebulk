const Sequelize = require('sequelize');
const fs =  require('fs');
const sequelize = new Sequelize('mysql://root:@localhost:3306/acamica');


fs.readFile('data.csv','utf8', (err, data) => {
    if (err) throw err;
    insertBulk(processCSV(data));
  });

function processCSV(fileContent){
    return fileContent.split('\r\n');
}

function insertBulk(data){
     data.forEach(element => {
        let info = element.split(',');
        insertAlumno(info);
    });
}

function insertAlumno(data){
    sequelize.query('INSERT INTO alumnos(nombre, email, telefono) VALUES (:nombre,:email,:telefono)', {replacements: {
        nombre: data[0],
        email: data[1],
        telefono: data[2]
    }
    }).then(function(resultados){
        console.log(resultados);
    });
}
